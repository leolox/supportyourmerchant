import { Component, OnInit } from '@angular/core';
import { GermanAddress } from '@angular-material-extensions/google-maps-autocomplete';
import { Router } from '@angular/router';
import { LocationService } from '../../../services/location.service'
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public locationService: LocationService,
    private router: Router,
    private _bottomSheet: MatBottomSheet
  ) { }

  isLoading = false;
  isLoggedIn = false;

  ngOnInit(): void {
  }

  onGermanAddressMapped($event: GermanAddress) {
    this.router.navigate([`/showMap/${$event.geoLocation.latitude}/${$event.geoLocation.longitude}`]);
  }

  useCurrentLocation() {
    this.isLoading = true;
    this.locationService.getPosition().then(pos => {
      this.isLoading = false;
      this.router.navigate([`/showMap/${pos.lat}/${pos.lng}`])
    })
      .catch(err => {
        console.error(err);
        this.isLoading = false;
      });

  }

  showDisclaimer() {
    this._bottomSheet.open(BottomSheetOverviewExampleSheet);
  }
}

@Component({
  selector: 'bottom-sheet-overview-example-sheet',
  templateUrl: 'bottom-sheet-overview-example-sheet.html',
})
export class BottomSheetOverviewExampleSheet {
  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewExampleSheet>) {}

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
}