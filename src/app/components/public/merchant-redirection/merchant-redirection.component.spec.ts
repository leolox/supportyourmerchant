import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantRedirectionComponent } from './merchant-redirection.component';

describe('MerchantRedirectionComponent', () => {
  let component: MerchantRedirectionComponent;
  let fixture: ComponentFixture<MerchantRedirectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantRedirectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantRedirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
