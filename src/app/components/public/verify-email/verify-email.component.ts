import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit(): void {
  }
  sendEmailVerification(){
    this.authService.SendVerificationMail().then((msg)=>{
      window.alert("Eine weitere Bestätigungsmail wurde versand, bitte überprüfe auch deinen Spam Ordner.");
    }).catch((err)=> {
      window.alert("Zu viele Versuche, bitte überprüfe deinen Spam Ordner!");
    });
   }

}
