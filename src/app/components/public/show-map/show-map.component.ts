import { Component, OnInit } from "@angular/core";
import { MouseEvent } from "@agm/core";
import { LocationService } from "../../../services/location.service";
import {
  AngularFirestoreDocument,
  AngularFirestore
} from "@angular/fire/firestore";
import { AngularFireStorage } from "@angular/fire/storage";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";

declare var google;
var infowindow;
var map;

@Component({
  selector: "app-show-map",
  templateUrl: "./show-map.component.html",
  styleUrls: ["./show-map.component.scss"]
})
export class ShowMapComponent implements OnInit {
  constructor(
    public afs: AngularFirestore,
    public locationService: LocationService,
    private route: ActivatedRoute
  ) {}
  subscriptions: Subscription[] = [];
  showMap = true;
  users = [];
  longitude = null;
  latitude = null;
  availablePlaces = [];

  ngOnInit(): void {
    this.longitude = this.route.snapshot.paramMap.get("lng");
    this.latitude = this.route.snapshot.paramMap.get("lat");

    infowindow = new google.maps.InfoWindow();
    this.initMap();
  }

  initMap() {
    var radius = 500;
    var currentPosition = new google.maps.LatLng(this.latitude, this.longitude);
    map = new google.maps.Map(document.getElementById("map"), {
      center: currentPosition,
      zoom: 15
    });

    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(
      { location: currentPosition, radius: radius, type: ["store"] },
      function(results, status, pagination) {
        if (status !== "OK") return;

        this.createMarkers(results);
      }.bind(this)
    );
  }

  getMerchants(place) {
    let merchantID = place.place_id;

    var merchantDoc: AngularFirestoreDocument<any> = this.afs.doc<any>(
      `/merchants/${merchantID}`
    );
    var merchant = merchantDoc.valueChanges();
    this.subscriptions.push(
      merchant.subscribe(data => {
        return data;
      })
    );
  }

  // google maps zoom level
  zoom: number = 8;

  // initial center position for the map
  lat: number = 51.673858;
  lng: number = 7.815982;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log("dragEnd", m, $event);
  }

  onRowClicked(place) {
    console.log(place);
  }

  createMarkers(places) {
    var bounds = new google.maps.LatLngBounds();
    console.log(places);
    for (var i = 0, place; (place = places[i]); i++) {
      // let data = this.getMerchants(place)
      // console.log('marker, place:',place,'data',data)
      var image = {
        url:
          "https://img.icons8.com/ultraviolet/80/000000/antivirus-scanner.png",
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(45, 45),
        background: "white"
      };

      let marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });
      marker.place = place
      // console.log(marker.place);

      let merchantID = place.place_id;

      var merchantDoc: AngularFirestoreDocument<any> = this.afs.doc<any>(
        `/merchants/${merchantID}`
      );
      var merchant = merchantDoc.valueChanges();
      this.subscriptions.push(
        merchant.subscribe(data => {
          if (data !== undefined) {
            marker.icon.url = "https://img.icons8.com/office/80/000000/antivirus-scanner.png";
          }
          google.maps.event.addListener(marker, "click", function() {
            if (data === undefined) {
              // console.log("place", marker.place)  ${marker.place.gmap.url}   TODO
              infowindow.setContent(
                `<div>
                <h2>${marker.title}</h2>
                <h3>Noch nicht bei Ladenliebe!</h3>
                <p>
                  Lade deinen Lieblingsladen ein!
                </p>
                <a href="https://www.google.com/maps/place/?q=place_id:${marker.place.place_id}">Lade den Ladenbesitzer ein und rette deinen Lieblingsladen!</a>
                <a href="/addLocation">Ich bin der Eigentümer und muss gerettet werden!</a>
              </div>`
              );
            } else {
              var date = new Date(data.closedsince.seconds*1000);
              var formattedTime = date.toLocaleDateString();

              infowindow.setContent(
                `<div>
                <h2>${marker.title}</h2>
                <h3>Geschlossen seit dem ${formattedTime}</h3>
                <p>
                  ${data.description}
                </p>
                <p> 
                  Helft mit <b>${data.valueMitarbeiter} Mitarbeiter</b> zu retten!
                </p>
                <p>
                  Website: 
                  <a href="${data.website}">${data.website}</a>
                </p>
                <p>
                  Findet uns bei Facebook: <a href="${data.facebook}">${data.facebook}</a>
                </p>
                <p>
                  Erreicht uns per Telefon ${data.tel}
                </p>
                <p>
                  Insgesamt brauchen wir <b>${data.maxvalue}€</b> Unterstützung!
                </p>
                <a href="/m/${marker.place.place_id}">Jetzt unterstützen</a>
              </div>`
              );
            }

            infowindow.open(map, this);
          });
        })
      );

      this.availablePlaces.push(place);
      bounds.extend(place.geometry.location);
    }
    map.fitBounds(bounds);
  }
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
