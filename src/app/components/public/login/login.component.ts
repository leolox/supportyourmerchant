import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    public authService: AuthService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

  onLoginSubmit(form){
    this.authService.login(form.form.value.email, form.form.value.password)
    .then(r => {
      console.log(r);
      if(r=== false){
        this.snackBar.open("Benutzername oder Passwort nicht korrekt", "🥴", {
          duration: 2000,
        });
      }

    }).catch(()=> {
      console.log("err");
    })
  }

}
