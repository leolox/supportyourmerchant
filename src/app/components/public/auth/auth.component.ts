import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserManagementActions } from './UserManagementActions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  ngUnsubscribe: Subject<any> = new Subject<any>();
  actions = UserManagementActions;

  // The user management actoin to be completed
  mode: string;
  // Just a code Firebase uses to prove that
  // this is a real password reset.
  actionCode: string;

  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  loginButtonActive: boolean = false;

  actionCodeChecked: boolean;
  actionCodeOLD: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public afAuth: AngularFireAuth,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(params => {
      // if we didn't receive any parameters,
      // we can't do anything
      if (!params) this.router.navigate(['/home']);

      this.mode = params['mode'];
      this.actionCode = params['oobCode'];
      console.log(this.mode);
      console.log(this.actionCode);

      switch (params['mode']) {
        case UserManagementActions.resetPassword: {
          console.log("switch resetPW")
          // Verify the password reset code is valid.
          this.authService
          .getAuth()
          .verifyPasswordResetCode(this.actionCode)
          .then(email => {
            this.actionCodeChecked = true;
          }).catch(e => {
            this.actionCodeOLD = true;
            // Invalid or expired action code. Ask user to try to
            // reset the password again.
            //alert(e);
            //console.log(e);

            //this.router.navigate(['/auth/login']);
          });
        } break
        case UserManagementActions.recoverEmail: {

        } break
        case UserManagementActions.verifyEmail: {
          console.log("verify email")

          this.authService.getAuth().applyActionCode(this.actionCode).then(msg => {
            console.log("apply action code: ", msg);
            this.actionCodeChecked = true;
            //this.router.navigate(['/dashboard'])
          });

          this.authService.getAuth().checkActionCode(this.actionCode).then(msg => {
            console.log("check action code:", msg);
          })

          //sign out
          this.afAuth.auth.signOut().then(() => {
            localStorage.removeItem('user');
          })
          //this.authService.SignOut();
        } break
        default: {
          console.log('query parameters are missing');
         // this.router.navigate(['/auth/login']);
        }
      }
    })
  }

  ngOnDestroy() {
    
    // End all subscriptions listening to ngUnsubscribe
    // to avoid memory leaks.
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }


  /**
   * Attempt to confirm the password reset with firebase and
   * navigate user back to home.
   */
  handleResetPassword() {
    if (this.newPassword != this.confirmPassword) {
      alert('Die beiden Passwörter sind nicht identisch.');
      return;
    }
    // Save the new password.
    this.authService.getAuth().confirmPasswordReset(
        this.actionCode,
        this.newPassword
    )
    .then(resp => {
      // Password reset has been confirmed and new password updated.
      //alert('Das neue Passwort wurde gespeichert');
      this.loginButtonActive = true;
      //this.router.navigate(['/auth/login']);
    }).catch(e => {
      // Error occurred during confirmation. The code might have
      // expired or the password is too weak.
      if(e.code == 'auth/weak-password'){
        alert("Das Passwort muss mindestens 6 Zeichen lang sein.")
      }else {
        alert("Es ist ein Fehler aufgetreten. Bitte versuche es erneut.")
      }
      console.log(e)

    });
  }

}
