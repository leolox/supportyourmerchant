import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  onErr= false;

  constructor(
    public authService: AuthService,
  ) { }

  ngOnInit(): void {
  }
  onRegisterSubmit(form){
    
    var userdata  = {
      firstname: form.form.value.firstname,
      lastname: form.form.value.lastname,
      email : form.form.value.email,
      phoneNumber: form.form.value.phoneNumber,
      providerId: "password"
    }
    console.log(form, userdata);
    
    this.authService.register(form.form.value.email, form.form.value.passwortone, userdata)
    .then((msg)=>  {
      console.log(msg, userdata);
      if(!msg){
        this.onErr = true;
      }
    })
    
  }

}
