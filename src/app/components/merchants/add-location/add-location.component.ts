import { Component, OnInit, OnDestroy } from '@angular/core';
import {Appearance, GermanAddress, Location} from '@angular-material-extensions/google-maps-autocomplete';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import { finalize } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.scss']
})
export class AddLocationComponent implements OnInit, OnDestroy {

  constructor(
    public afs: AngularFirestore,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    public authService: AuthService,
    private router: Router
  ) { }

  autoAdress: any;
  subscriptions: Subscription[] = [];
  uid: string;

  ngOnInit(): void {
    var authService: any = this.authService.getAuth()
    this.uid= authService.W;
    this.setDate();
  }

  ngOnDestroy(){
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

 
  onGermanAddressMapped($event: GermanAddress) {
    console.log('onGermanAddressMapped', $event);
    this.autoAdress = $event;
  }
  test: any;
  onSubmit(form){
    //checks todo: checks(date!)
    console.log(this.test)
    console.log(form);
    
    this.saveToDB(form.form.value)
    .then((response)=> {
      console.log(response);

      //save to user
      const userDoc = this.afs.doc(`/users/${this.uid}`);
      userDoc.set({locations: [this.autoAdress.placeID]}, {merge: true} )
      .then((r)=> {
        console.log(r);
        //alles gut gegangen

        this.router.navigate([`/m/${this.autoAdress.placeID}`])

        this.snackBar.open("Wohin soll es gehen?", "😊", {
          duration:  2*1000,
          });

      }).catch((err)=> {
        console.log(err);
        this.snackBar.open("fehler wurden begangen", "😩", {
          duration:  2*1000,
          });
      })

    }).catch((err)=> {

      this.snackBar.open("fehler wurden begangen", "😩", {
      duration:  2*1000,
      });
    })
    
  }

  saveToDB(data){
    console.log(data);
    const merchantsCollection = this.afs.collection<any>('merchants');

    var dbData : any = {
      gmap: this.autoAdress,
      name: data.name,
      commonName: data.commonName,
      tel: data.tel,
      maxvalue: data.maxvalue,
      paypal: data.paypal,
      umsID: data.umsID,
      handNmr: data.handNmr,
      website: data?.website || null,
      facebook: data?.facebook || null,
      logoURL:  this.finalLogoUrl || null,
      logoPath: `/merchants/${this.autoAdress.placeID}/logo`,
      valueMitarbeiter: data?.employeeCount || null,
      description: data?.description,
      closedsince: this.closedSince,
      heroImg: this.finalHeroUrl || null,
      heroImgPath: `/merchants/${this.autoAdress.placeID}/hero`,
      owner: this.uid,
      voucher: this.voucher
    }

   return merchantsCollection.doc(dbData.gmap.placeID).set(dbData, {merge: true});
  }

 closedSince: Date;
 setClosedSinceDate(a,event){
   console.log(event);
  this.closedSince = event.value;
 }

 // set current date on datepicker
 setDate(){
 }

 
 imageURL: string;
 uploadPercent: Observable<number>;
 downloadURL: Observable<string>;
 uploadLogo(event) {
  const file = event.target.files[0];
  const filePath = `/merchants/${this.autoAdress.placeID}/logo`;

  const fileRef = this.storage.ref(filePath);
  const task = this.storage.upload(filePath, file);

  // observe percentage changes
  this.uploadPercent = task.percentageChanges();
  // get notified when the download URL is available
  task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();  
        this.logoUrl();
       })
   )
  .subscribe()
  }

  finalLogoUrl: string;
  logoUrl(){
    this.subscriptions.push(
      this.downloadURL.subscribe(url => {
        this.finalLogoUrl = url;
      })
    )
  }

  heroDonwloadURL: Observable<string>;
  uploadHero(event){
    const file = event.target.files[0];
    const filePath = `/merchants/${this.autoAdress.placeID}/hero`;
  
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
  
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(() => {
          this.heroDonwloadURL = fileRef.getDownloadURL();  
          this.heroUrl();
         })
     )
    .subscribe()

  }

  finalHeroUrl: string;
  heroUrl(){
    this.subscriptions.push(
      this.heroDonwloadURL.subscribe(url => {
        this.finalLogoUrl = url;
      })
    )
  }

  voucher: boolean = false;
  onVoucherChange(event){
    this.voucher = event.checked;
  }



}
