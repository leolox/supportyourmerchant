import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantMapComponent } from './merchant-map.component';

describe('MerchantMapComponent', () => {
  let component: MerchantMapComponent;
  let fixture: ComponentFixture<MerchantMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
