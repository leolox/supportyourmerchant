import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-show-merchant',
  templateUrl: './show-merchant.component.html',
  styleUrls: ['./show-merchant.component.scss']
})
export class ShowMerchantComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  merchantData: any;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private afs: AngularFirestore
  ) { }


  merchantID: string;
  ngOnInit(): void {
    this.merchantID = this.route.snapshot.paramMap.get('merchant');
    //console.log(this.merchantID);

    this.getDBdata();
    this.getDonationValue();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  getDBdata(){
    var merchantDoc: AngularFirestoreDocument<any> = this.afs.doc<any>(`/merchants/${this.merchantID}`);
    var merchant = merchantDoc.valueChanges();
    this.subscriptions.push(
      merchant.subscribe((data)=> {
        console.log(data);
        if(data === undefined){
          //err
          this.router.navigate([`/`])
        }else{
          this.merchantData = data;
        }
      })
    )
  }

  customValue: number;
  onSubmit(value, custom){
    if(custom != ''){
      window.open(`${this.merchantData.paypal}/${this.customValue}`,'_blank','noopener');
      this.saveTransaction(this.customValue);
    }else{
      window.open(`${this.merchantData.paypal}/${value}`,'_blank','noopener');
      this.saveTransaction(value);
    }
  }

  saveTransaction(value){
    const merchantCollection = this.afs.collection<any>(`merchants/${this.merchantID}/donations/`);
    const now = Date.now();
    var data: any = {
      transactionDate: now,
      amount: value
    }

    
    merchantCollection.add(data)
    .then(()=> {
      console.log("sucess")
      //update global count
      const sum = this.afs.doc<any>(`merchants/${this.merchantID}/donations/sum`);
      sum.update( {count: firebase.firestore.FieldValue.increment(value)})
      .then(()=> {

      }).catch(()=> {
        //create doc 
        sum.set({count: value});
      })

    }).catch(()=> {

    });
  }

  donationValue;
  getDonationValue(){

    const donationDoc: AngularFirestoreDocument<any> = this.afs.doc<any>(`merchants/${this.merchantID}/donations/sum`);
    const donation = donationDoc.valueChanges();

    this.subscriptions.push(
      donation.subscribe((data:any)=> {
        this.donationValue = data?.count;
      })
    )

  }


}
