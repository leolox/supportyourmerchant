import { Component, OnInit, OnDestroy } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { GermanAddress } from '@angular-material-extensions/google-maps-autocomplete';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthService } from 'src/app/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-change-location',
  templateUrl: './change-location.component.html',
  styleUrls: ['./change-location.component.scss']
})
export class ChangeLocationComponent implements OnInit, OnDestroy {
   //now = Date.now();
   date = new FormControl(new Date());

  constructor(
    public afs: AngularFirestore,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    public authService: AuthService,
    private router: Router
  ) { }

  autoAdress: any;
  subscriptions: Subscription[] = [];
  uid: string;
  merchantID: string = "ChIJkZGDs75RqEcR2Xn5wbwB9MY";
  merchantData: any = {};
  closedSinceFromControl: FormControl;

  ngOnInit(): void {
    var authService: any = this.authService.getAuth()
    this.uid= authService.W;
    this.setDate();
    this.getDatafromDB();
    //this.merchantID = JSON.parse(sessionStorage.getItem('merchantID'))
    
  }

  ngOnDestroy(){
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  getDatafromDB(){
    const merchantDoc: AngularFirestoreDocument<any> = this.afs.doc<any>(`/merchants/${this.merchantID}`);
    var merchant = merchantDoc.valueChanges();
    this.subscriptions.push(
      merchant.subscribe((data)=> {
        console.log(data);
        if(data === undefined){
          //err
          this.router.navigate([`/`])
        }else{
          this.merchantData = data;
          this.closedSinceFromControl = new FormControl(new Date(this.merchantData.closedsince.seconds*1000));
          console.log(this.merchantData)
        }
      })
    )
  }

 
  onGermanAddressMapped($event: GermanAddress) {
    console.log('onGermanAddressMapped', $event);
    this.autoAdress = $event;
  }
  test: any;
  onSubmit(){
    //checks todo: checks(date!)
    console.log(this.test)
    console.log(this.merchantData);
    
    this.saveToDB(this.merchantData)
    .then((response)=> {
      console.log(response);

      //save to user
      const userDoc = this.afs.doc(`/users/${this.uid}`);
      userDoc.set({locations: [this.merchantID]}, {merge: true} )
      .then((r)=> {
        console.log(r);
        //alles gut gegangen

        this.router.navigate([`/m/${this.merchantID}`])

        this.snackBar.open("Wohin soll es gehen?", "😊", {
          duration:  2*1000,
          });

      }).catch((err)=> {
        console.log(err);
        this.snackBar.open("fehler wurden begangen", "😩", {
          duration:  2*1000,
          });
      })

    }).catch((err)=> {

      this.snackBar.open("fehler wurden begangen", "😩", {
      duration:  2*1000,
      });
    })
    
  }

  saveToDB(data){
    console.log("datajbr", data);
    const merchantsCollection = this.afs.collection<any>('merchants');
    var dclosed=this.closedSince;
    if (this.closedSince == undefined) {
      dclosed = this.merchantData.closedsince
    }
    var dbData : any = {
      name: data.name,
      commonName: data.commonName,
      tel: data.tel,
      maxvalue: data.maxvalue,
      paypal: data.paypal,
      website: data?.website || null,
      facebook: data?.facebook || null,
      logoURL:  this.finalLogoUrl || null,
      logoPath: `/merchants/${this.merchantID}/logo`,
      valueMitarbeiter: data?.valueMitarbeiter || null,
      description: data?.description,
      closedsince: dclosed,
      heroImg: this.finalHeroUrl || null,
      heroImgPath: `/merchants/${this.merchantID}/hero`,
      owner: this.uid,
      voucher: this.voucher
    }

   return merchantsCollection.doc(this.merchantID).set(dbData, {merge: true});
  }

 closedSince : Date;
 setClosedSinceDate(a,event){
   console.log(event);
  this.closedSince = event.value;
 }

 // set current date on datepicker
 setDate(){
 }

 
 imageURL: string;
 uploadPercent: Observable<number>;
 downloadURL: Observable<string>;
 uploadLogo(event) {
  const file = event.target.files[0];
  const filePath = `/merchants/${this.autoAdress.placeID}/logo`;

  const fileRef = this.storage.ref(filePath);
  const task = this.storage.upload(filePath, file);

  // observe percentage changes
  this.uploadPercent = task.percentageChanges();
  // get notified when the download URL is available
  task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();  
        this.logoUrl();
       })
   )
  .subscribe()
  }

  finalLogoUrl: string;
  logoUrl(){
    this.subscriptions.push(
      this.downloadURL.subscribe(url => {
        this.finalLogoUrl = url;
      })
    )
  }

  heroDonwloadURL: Observable<string>;
  uploadHero(event){
    const file = event.target.files[0];
    const filePath = `/merchants/${this.autoAdress.placeID}/hero`;
  
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
  
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(() => {
          this.heroDonwloadURL = fileRef.getDownloadURL();  
          this.heroUrl();
         })
     )
    .subscribe()

  }

  finalHeroUrl: string;
  heroUrl(){
    this.subscriptions.push(
      this.heroDonwloadURL.subscribe(url => {
        this.finalLogoUrl = url;
      })
    )
  }

  voucher: boolean = false;
  onVoucherChange(event){
    this.voucher = event.checked;
  }



}

