import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


// Material //
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatMenu, MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';



import { MatToolbarModule } from '@angular/material/toolbar';

import {MatSelectModule} from '@angular/material/select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HomeComponent } from './components/public/home/home.component';
import { DashboardComponent } from './components/merchants/dashboard/dashboard.component';
import { LoginComponent } from './components/public/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import {FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirePerformanceModule } from '@angular/fire/performance';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { AddLocationComponent } from './components/merchants/add-location/add-location.component';
import { RegisterComponent } from './components/public/register/register.component';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { AgmCoreModule } from '@agm/core';
import { ShowMerchantComponent } from './components/merchants/show-merchant/show-merchant.component';
import { MerchantMapComponent } from './components/merchants/merchant-map/merchant-map.component';
import { MatNativeDateModule } from '@angular/material/core';
import { AuthComponent } from './components/public/auth/auth.component';
import { VerifyEmailComponent } from './components/public/verify-email/verify-email.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';






import { ShowMapComponent } from './components/public/show-map/show-map.component';
import { LocationService } from './services/location.service';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MerchantRedirectionComponent } from './components/public/merchant-redirection/merchant-redirection.component';
import { NavComponent } from './components/public/nav/nav.component';
import { ChangeLocationComponent } from './components/merchants/change-location/change-location.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    LoginComponent,
    AddLocationComponent,
    RegisterComponent,
    AuthComponent,
    VerifyEmailComponent,
    ShowMerchantComponent,
    MerchantMapComponent,
    ShowMapComponent,
    MerchantRedirectionComponent,
    NavComponent,
    ChangeLocationComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireFunctionsModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirePerformanceModule,
    AngularFireAuthGuardModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatDialogModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatSlideToggleModule,
    MatBottomSheetModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatGoogleMapsAutocompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCXiDOtat-gBQjIqNvR2Tm0t2AlCG1mjDs',
      libraries: ['places']
    }),
  ],
  providers: [
    [AuthService,
    MatDatepickerModule,
	LocationService],
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
