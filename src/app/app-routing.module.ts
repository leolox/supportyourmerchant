import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/public/home/home.component';
import { LoginComponent } from './components/public/login/login.component';
import { AngularFireAuthGuard, customClaims, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { DashboardComponent } from './components/merchants/dashboard/dashboard.component';
import { pipe, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { AddLocationComponent } from './components/merchants/add-location/add-location.component';
import { ShowMapComponent } from './components/public/show-map/show-map.component';
import { RegisterComponent } from './components/public/register/register.component';
import { ShowMerchantComponent } from './components/merchants/show-merchant/show-merchant.component';
import { AuthComponent } from './components/public/auth/auth.component';
import { VerifyEmailComponent } from './components/public/verify-email/verify-email.component'
import { MerchantRedirectionComponent } from './components/public/merchant-redirection/merchant-redirection.component'
import { ChangeLocationComponent } from './components/merchants/change-location/change-location.component';

// authGuardPipes // 

//demo (no effect)
const isAdmin = () => pipe(customClaims, map(claims => {
  if (claims.admin) {
    return true; // ok, go to your route
  } else if (claims.user) {
    return ['/whereUsersAreRedirected']; // users with claim user are redirected
  } else {
    return [''];
  }
}));

const isUser = () => pipe(map(user => {
  return !!user ? !!user : ['/merchantRedirect'];
}));
const redirectUnauthorizedToLogin = redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: '', component: HomeComponent },

  { path: 'auth', component: AuthComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'm/:merchant', component: ShowMerchantComponent },
  { path: 'merchantRedirect', component: MerchantRedirectionComponent },
  { path: 'showMap/:lat/:lng', component: ShowMapComponent, },

  // protected routes
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: isUser }
  },
  {
    path: 'addLocation',
    component: AddLocationComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: isUser }
  },
  {
    path: 'changeLocation',
    component: ChangeLocationComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: isUser }
  },

  { path: '**', redirectTo: '' }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

