export interface User {
    uid: string,
    displayName: string,
    email: string,
    phoneNumber: string,
    photoURL: string,
    providerId: string,
    firstname?: string,
    lastname?:string
}