// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA-zV2wrePr8vNxUHdUnTQ4aSc6rkj8mkM",
    authDomain: "supportyourmerchant.firebaseapp.com",
    databaseURL: "https://supportyourmerchant.firebaseio.com",
    projectId: "supportyourmerchant",
    storageBucket: "supportyourmerchant.appspot.com",
    messagingSenderId: "284902689612",
    appId: "1:284902689612:web:c1a23323d152ce23f51de9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
